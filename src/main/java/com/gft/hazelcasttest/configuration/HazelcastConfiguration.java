package com.gft.hazelcasttest.configuration;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HazelcastConfiguration {

    @Value("${hazelcast.name}")
    private String hazelcastName;

    //this doesn't create cluster
    @Bean(destroyMethod = "shutdown")
    public HazelcastInstance hazelcastInstance() {
        ClientConfig config = new ClientConfig();
        config.setInstanceName(hazelcastName);
        config.getGroupConfig().setName(hazelcastName);
        HazelcastInstance instance = HazelcastClient.newHazelcastClient(config);
        return instance;
    }

    //this creates cluster
//    @Bean(destroyMethod = "shutdown")
//    public HazelcastInstance hazelcastInstance() {
//        Config config = new Config();
//        config.setInstanceName(hazelcastName);
//        config.getGroupConfig().setName(hazelcastName);
//        HazelcastInstance instance = Hazelcast.newHazelcastInstance(config);
//        return instance;
//    }
}
