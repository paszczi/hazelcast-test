package com.gft.hazelcasttest.web;

import com.gft.hazelcasttest.service.AlternativeMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

    private static final String user = "ME";

    @Autowired
    //private MessageService service;
    private AlternativeMessageService service;

    @GetMapping("/api/messages")
    public String getMessage() {
        return service.getMessage(user);
    }

    @PostMapping
    public void setMessage(@RequestBody String newMessage) {
        service.changeMessage(user, newMessage);
    }
}
