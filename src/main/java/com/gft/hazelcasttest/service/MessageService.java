package com.gft.hazelcasttest.service;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = MessageService.CACHE_NAME)
public class MessageService {

    public static final String CACHE_NAME = "message_service_cache";

    private String message;

    @Cacheable(key="#username")
    public String getMessage(String username) {
        return message;
    }

    @CachePut(key="#username")
    public String changeMessage(String username, String newMessage) {
        message = newMessage;
        return message;
    }
}
