package com.gft.hazelcasttest.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.springframework.stereotype.Service;

@Service
public class AlternativeMessageService {

    private final IMap<String, String> my_messages;

    public AlternativeMessageService(HazelcastInstance hazelcastInstance) {
        my_messages = hazelcastInstance.getMap("MY_MESSAGES");
    }

    public String getMessage(String username) {
        return my_messages.get(username);
    }

    public void changeMessage(String username, String newMessage) {
        my_messages.set(username, newMessage);
    }
}
